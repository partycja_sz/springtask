package com.challenge.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.challenge.dao.StatisticDao;
import com.challenge.mapper.StatisticMapper;
import com.challenge.mapper.dto.StatisticDto;
import com.challenge.service.StatisticService;

public class StatisticServiceImpl implements StatisticService {

	private StatisticMapper statisticMapper;
	private StatisticDao statisticDao;

	@Autowired
	public StatisticServiceImpl(StatisticMapper statisticMapper, StatisticDao statisticDao) {
		this.statisticMapper = statisticMapper;
		this.statisticDao = statisticDao;
	}

	@Override
	public Optional<StatisticDto> showStatistics(Long idUser) {
		return statisticDao.showStatistics(idUser);
	}
}
