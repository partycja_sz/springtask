package com.challenge.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

import com.challenge.dao.GameHistoryDao;
import com.challenge.entity.GameHistory;
import com.challenge.mapper.GameHistoryMapper;
import com.challenge.mapper.dto.GameHistoryDto;
import com.challenge.service.GameHistoryService;

public class GameHistoryServiceImpl implements GameHistoryService {

	private GameHistoryMapper mapper;
	private GameHistoryDao gameHistoryDao;

	@Autowired
	public GameHistoryServiceImpl(GameHistoryMapper mapper, GameHistoryDao gameHistoryDao) {
		this.mapper = mapper;
		this.gameHistoryDao = gameHistoryDao;
	}

	@Override
	public List<GameHistory> showGameHistoryForUser(Long idUser) {
		return gameHistoryDao.showGameHistoryForUser(idUser);
	}

	@Override
	public void addGameToHistory(GameHistoryDto gameHistoryDto) {
		gameHistoryDao.addGameToHistory(gameHistoryDto);
	}
}
