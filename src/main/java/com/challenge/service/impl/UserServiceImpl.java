package com.challenge.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.challenge.aspect.LoggingAnnotation;
import com.challenge.dao.GameBoardDao;
import com.challenge.dao.UserDao;
import com.challenge.entity.User;
import com.challenge.exceptions.DuplicatedGameException;
import com.challenge.mapper.UserMapper;
import com.challenge.mapper.dto.UserDto;
import com.challenge.service.UserService;
import com.challenge.service.validator.UserServiceValidator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserServiceImpl implements UserService {

	private UserMapper mapper;
	private UserDao userDao;
	private GameBoardDao gameBoardDao;
	private UserServiceValidator userServiceValidator;

	@Autowired
	public UserServiceImpl(UserMapper mapper, UserDao userDao, GameBoardDao gameBoardDao,
			UserServiceValidator userServiceValidator) {
		this.mapper = mapper;
		this.userDao = userDao;
		this.gameBoardDao = gameBoardDao;
		this.userServiceValidator = userServiceValidator;
	}
	
	@LoggingAnnotation
	@Override
	public Long addUser(UserDto userDto) {
		log.info(UserServiceImpl.class.getName().toString() + "addUser(UserDto userDto");
		log.info("User '{} {}' has been added.", userDto.getFirstName(), userDto.getLastName());
		return userDao.save(userDto);
	}

	@LoggingAnnotation
	@Override
	public Long updateUserData(UserDto userDto) {
		log.info(UserServiceImpl.class.getName().toString() + "updateUserData(UserDto userDto)");
		log.info("User's email '{}' has been updated.", userDto.getEmail());
		return userDao.updateUserData(userDto);
	}

	@LoggingAnnotation
	@Override
	public Long addGame(Long idUser, Long idGameToAdd) throws DuplicatedGameException {
		log.info("User '{} {}' has new game: {} added into own collection.",
				userDao.findById(idUser).get().getFirstName(), userDao.findById(idUser).get().getLastName(),
				gameBoardDao.findById(idGameToAdd).get().getName());
		if (userServiceValidator.validateGameDuplication(idUser, idGameToAdd)) {
			throw new DuplicatedGameException();
		}
		return userDao.addGameToOwnCollection(idUser, idGameToAdd);
	}

	@LoggingAnnotation
	@Override
	public Optional<UserDto> showProfileInfo(UserDto userDto) {
		return userDao.showProfileInfo(userDto);
	}

	@LoggingAnnotation
	@Override
	public List<Long> showUserGameCollection(Long userId) {
		return userDao.showUserGameCollection(userId);
	}

	@LoggingAnnotation
	@Override
	public List<Long> removeGameFromCollection(Long idUser, Long idGameToAdd) {
		return userDao.removeGameFromCollection(idUser, idGameToAdd);
	}

	@LoggingAnnotation
	@Override
	public List<User> findAllUsers() {
		return userDao.findAllUsers();
	}
}
