package com.challenge.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.challenge.dao.GameBoardDao;
import com.challenge.entity.GameBoard;
import com.challenge.exceptions.ToLongGameNameException;
import com.challenge.mapper.GameBoardMapper;
import com.challenge.mapper.dto.GameBoardDto;
import com.challenge.service.GameBoardService;
import com.challenge.service.validator.GameServiceValidator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GameBoardServiceImpl implements GameBoardService {

	private GameBoardMapper mapper;
	private GameBoardDao gameDao;
	private GameServiceValidator gameServiceValidator;

	@Autowired
	public GameBoardServiceImpl(GameBoardMapper mapper, GameBoardDao gameDao, GameServiceValidator gameServiceValidator) {
		this.mapper = mapper;
		this.gameDao = gameDao;
		this.gameServiceValidator = gameServiceValidator;
	}

	@Override
	public Long addGame(GameBoardDto gameDto) throws ToLongGameNameException {
		log.info("Game '{}' has been added.", gameDto.getName());
		if (gameServiceValidator.validateGameNameLength(gameDto)) {
			throw new ToLongGameNameException();
		}
		return gameDao.save(gameDto);
	}

	@Override
	public List<GameBoard> findAllGames() {
		return gameDao.findAllGames();
	}

}
