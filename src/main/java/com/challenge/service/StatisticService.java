package com.challenge.service;

import java.util.Optional;

import com.challenge.mapper.dto.StatisticDto;

public interface StatisticService {

	Optional<StatisticDto> showStatistics(Long idUser);
}
