package com.challenge.service;

import java.util.List;

import com.challenge.entity.GameBoard;
import com.challenge.exceptions.ToLongGameNameException;
import com.challenge.mapper.dto.GameBoardDto;

public interface GameBoardService {
	Long addGame(GameBoardDto gameDto) throws ToLongGameNameException;

	List<GameBoard> findAllGames();
}
