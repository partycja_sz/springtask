package com.challenge.service.validator;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.challenge.exceptions.ToLongGameNameException;
import com.challenge.mapper.dto.GameBoardDto;

@Component
public class GameServiceValidator {
	
	@Value("${max.game.name.length}")
    private int maxGameNameLength;

	public boolean validateGameNameLength(GameBoardDto gameDto) throws ToLongGameNameException {
		if (gameDto.getName().length()>maxGameNameLength)
			return true;
		return false;
	}
}
