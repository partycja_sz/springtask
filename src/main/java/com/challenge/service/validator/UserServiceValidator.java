package com.challenge.service.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.challenge.dao.UserDao;

@Component
public class UserServiceValidator {

	@Autowired
	private UserDao userDao;

	public boolean validateGameDuplication(Long idUser, Long idGameToAdd) {
		if (userDao.findById(idUser).get().getGameUserCollection() != null
				&& userDao.findById(idUser).get().getGameUserCollection().contains(idGameToAdd))
			return true;
		return false;
	}
}
