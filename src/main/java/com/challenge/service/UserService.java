package com.challenge.service;

import java.util.List;
import java.util.Optional;

import com.challenge.entity.User;
import com.challenge.exceptions.DuplicatedGameException;
import com.challenge.mapper.dto.UserDto;

public interface UserService {
	Long addUser(UserDto userDto);

	Long updateUserData(UserDto userDto);

	Long addGame(Long idUser, Long idGameToAdd) throws DuplicatedGameException;

	Optional<UserDto> showProfileInfo(UserDto userDto);

	List<Long> showUserGameCollection(Long userId);

	List<Long> removeGameFromCollection(Long idUser, Long idGameToAdd);

	List<User> findAllUsers();
}
