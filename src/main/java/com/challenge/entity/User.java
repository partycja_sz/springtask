package com.challenge.entity;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class User {
	private static Long NUM = 0L;
	private String firstName;
	private String lastName;
	private String email;
	private String motto;
	private String password;
	private Long id;
	private List<Long> gameUserCollection;

	public User(String firstName, String lastName, String email, String motto, String password) {
		this.id = NUM++;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.motto = motto;
		this.password = password;
	}

	public User(String firstName, String lastName, String email, String motto, String password,
			List<Long> gameUserCollection) {
		this.id = NUM++;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.motto = motto;
		this.password = password;
		this.gameUserCollection = gameUserCollection;
	}

	public User() {
		this.id = NUM++;
	}
}
