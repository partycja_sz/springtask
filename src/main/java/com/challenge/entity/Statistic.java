package com.challenge.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Statistic {

	private Long level;
	private Long rankingPosition;
	private Long winGamesStatistics;
	private Long userId;

	public Statistic(Long userId, Long level, Long rankingPosition, Long winGamesStatistics) {
		this.userId = userId;
		this.level = level;
		this.rankingPosition = rankingPosition;
		this.winGamesStatistics = winGamesStatistics;
	}

	public Statistic() {
	}
}
