package com.challenge.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GameBoard {
	private static Long NUM = 0L;
	private String name;
	private Long id;

	public GameBoard(String name) {
		this.id = NUM++;
		this.name = name;
	}

	public GameBoard() {
		this.id = NUM++;
	}
}
