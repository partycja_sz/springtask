package com.challenge.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GameHistory {
	private Long idFirstPlayer;
	private Long idSecondPlayer;
	private Long idGame;

	public GameHistory(Long idFirstPlayer, Long idSecondPlayer, Long idGame) {
		this.idFirstPlayer = idFirstPlayer;
		this.idSecondPlayer = idSecondPlayer;
		this.idGame = idGame;
	}

	public GameHistory() {
	}
}
