package com.challenge.dao;

import java.util.List;
import java.util.Optional;

import com.challenge.entity.GameHistory;
import com.challenge.mapper.dto.GameHistoryDto;

public interface GameHistoryDao {
	List<GameHistory> showGameHistoryForUser(Long idUser);

	void addGameToHistory(GameHistoryDto gameHistoryDto);
}
