package com.challenge.dao;

import java.util.List;
import java.util.Optional;

import com.challenge.entity.GameBoard;
import com.challenge.mapper.dto.GameBoardDto;

public interface GameBoardDao {
	Optional<GameBoardDto> findById(Long id);

	Long save(GameBoardDto gameDto);

	List<GameBoard> findAllGames();
}
