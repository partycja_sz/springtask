package com.challenge.dao;

import java.util.Optional;

import com.challenge.mapper.dto.StatisticDto;

public interface StatisticDao {

	Optional<StatisticDto> showStatistics(Long idUser);
}
