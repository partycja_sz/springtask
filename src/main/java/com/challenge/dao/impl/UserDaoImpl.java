package com.challenge.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.challenge.dao.GameBoardDao;
import com.challenge.dao.UserDao;
import com.challenge.entity.User;
import com.challenge.mapper.GameBoardMapper;
import com.challenge.mapper.UserMapper;
import com.challenge.mapper.dto.UserDto;

@Repository
public class UserDaoImpl implements UserDao {

	private UserMapper mapper;
	private GameBoardMapper gameMapper;
	private GameBoardDao gameDao;

	private static List<User> users;

	static {
		resetUsers();
	}

	private static void resetUsers() {
		users = new ArrayList<>();
		List<Long> user4Games = new ArrayList<Long>();
		List<Long> user5Games = new ArrayList<Long>();
		List<Long> user6Games = new ArrayList<Long>();
		List<Long> user7Games = new ArrayList<Long>();
		Long game1 = 1L;
		Long game2 = 2L;
		Long game3 = 3L;
		user4Games.add(game1);
		user4Games.add(game3);
		user5Games.add(game2);
		user5Games.add(game1);
		user6Games.add(game1);
		user6Games.add(game2);
		user7Games.add(game1);
		users.add(new User("Adam", "One", "adam.one@capgemini.jstk.com", "Be happy", "12345"));
		users.add(new User("Eva", "Second", "eva.second@capgemini.jstk.com", "Great day to win", "password"));
		users.add(new User("Clyde", "Third", "clyde.third@capgemini.jstk.com", "Always look at the bright side of life",
				"54321"));
		users.add(new User("Rick", "Nick", "rick.nick@capgemini.jstk.com", "Brand new day", "54321", user4Games));
		users.add(new User("Nicolas", "Great", "nicolas.great@capgemini.jstk.com", "Spring made my day", "123456",
				user5Games));
		users.add(new User("Nicolas", "Great", "nicolas.great@capgemini.jstk.com", "Spring made my day", "123456",
				user6Games));
		users.add(
				new User("Nick", "Break", "nick.break@capgemini.jstk.com", "Spring made my day", "123456", user7Games));
	}

	@Autowired
	public UserDaoImpl(UserMapper mapper) {
		this.mapper = mapper;
	}

	@Override
	public Optional<UserDto> findById(Long id) {
		Optional<User> first = findUserById(id);
		return mapper.map(first);
	}

	@Override
	public Long save(UserDto userDto) {
		User user = mapper.map(userDto);
		Optional<User> foundUser = findUserById(user.getId());
		if (foundUser.isPresent()) {
			users.remove(foundUser.get());
			user = foundUser.get();
		}
		users.add(user);
		return user.getId();
	}

	@Override
	public Long updateUserData(UserDto userDto) {
		int i = userDto.getId().intValue();
		Optional<User> foundUser = findUserById(mapper.map(userDto).getId());
		User user = mapper.map(userDto);
		users.set(i, user);
		return foundUser.get().getId();
	}

	private Optional<User> findUserById(Long id) {
		return users.stream().filter(o -> o.getId().equals(id)).findFirst();
	}

	@Override
	public Long addGameToOwnCollection(Long idUser, Long idGameToAdd) {
		Optional<User> foundUser = findUserById(idUser);
		ArrayList<Long> userGameBoardsList = new ArrayList<>();
		userGameBoardsList.add(idGameToAdd);
		foundUser.get().setGameUserCollection(userGameBoardsList);
		users.set(idUser.intValue(), foundUser.get());
		return foundUser.get().getId();
	}

	@Override
	public Optional<UserDto> showProfileInfo(UserDto userDto) {
		int i = userDto.getId().intValue();
		Optional<UserDto> userDto2 = mapper.map(Optional.ofNullable(users.get(i)));
		return userDto2;
	}

	@Override
	public List<Long> showUserGameCollection(Long userId) {
		return users.get(userId.intValue()).getGameUserCollection();
	}

	@Override
	public List<Long> removeGameFromCollection(Long idUser, Long idGameToAdd) {
		Optional<User> foundUser = findUserById(idUser);
		List<Long> userGameBoardsList = new ArrayList<>();
		userGameBoardsList = foundUser.get().getGameUserCollection();
		userGameBoardsList.remove(idGameToAdd);
		foundUser.get().setGameUserCollection(userGameBoardsList);
		users.set(idUser.intValue(), foundUser.get());
		return users.get(idUser.intValue()).getGameUserCollection();
	}

	@Override
	public List<User> findAllUsers() {
		return users.stream().collect(Collectors.toList());
	}
}
