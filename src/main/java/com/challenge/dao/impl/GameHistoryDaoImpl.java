package com.challenge.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.challenge.dao.GameHistoryDao;
import com.challenge.entity.GameHistory;
import com.challenge.entity.User;
import com.challenge.mapper.GameHistoryMapper;
import com.challenge.mapper.dto.GameHistoryDto;

@Repository
public class GameHistoryDaoImpl implements GameHistoryDao {
	
	private GameHistoryMapper gameHistoryMapper;
	private static List<GameHistory> gameHistories;

	@Autowired
	public GameHistoryDaoImpl(GameHistoryMapper gameHistoryMapper) {
		this.gameHistoryMapper = gameHistoryMapper;
	}

	static {
		resetHistory();
	}

	private static void resetHistory() {
		gameHistories = new ArrayList<>();
		gameHistories.add(new GameHistory(1L, 2L, 0L));
		gameHistories.add(new GameHistory(2L, 3L, 1L));
		gameHistories.add(new GameHistory(3L, 4L, 2L));
		gameHistories.add(new GameHistory(4L, 3L, 3L));
		gameHistories.add(new GameHistory(1L, 3L, 4L));
		gameHistories.add(new GameHistory(1L, 4L, 5L));
		gameHistories.add(new GameHistory(2L, 1L, 5L));
	}
	
	@Override
	public List<GameHistory> showGameHistoryForUser(Long idUser) {
		return gameHistories.stream()
				.filter(history -> (history.getIdFirstPlayer().equals(idUser) || history.getIdSecondPlayer().equals(idUser)))
				.collect(Collectors.toList());
	}

	@Override
	public void addGameToHistory(GameHistoryDto gameHistoryDto) {
		GameHistory gameHistory = gameHistoryMapper.map(gameHistoryDto);
		gameHistories.add(gameHistory);
	}
}
