package com.challenge.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.challenge.dao.StatisticDao;
import com.challenge.entity.Statistic;
import com.challenge.mapper.StatisticMapper;
import com.challenge.mapper.dto.StatisticDto;

@Repository
public class StatisticDaoImpl implements StatisticDao {

	private StatisticMapper statisticMapper;
	private static List<Statistic> statistics;

	@Autowired
	public StatisticDaoImpl(StatisticMapper statisticMapper) {
		this.statisticMapper = statisticMapper;
	}

	static {
		resetStatistic();
	}

	private static void resetStatistic() {
		statistics = new ArrayList<>();
		statistics.add(new Statistic(1L, 2L, 4L, 3L));
		statistics.add(new Statistic(2L, 2L, 3L, 5L));
		statistics.add(new Statistic(3L, 3L, 2L, 7L));
		statistics.add(new Statistic(4L, 3L, 1L, 10L));
	}

	@Override
	public Optional<StatisticDto> showStatistics(Long idUser) {
		return statisticMapper
				.map(statistics.stream().filter(userStatistic -> userStatistic.getUserId().equals(idUser)).findFirst());
	}

}
