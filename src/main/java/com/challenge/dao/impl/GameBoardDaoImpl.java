package com.challenge.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.challenge.dao.GameBoardDao;
import com.challenge.entity.GameBoard;
import com.challenge.mapper.GameBoardMapper;
import com.challenge.mapper.dto.GameBoardDto;

@Repository
public class GameBoardDaoImpl implements GameBoardDao {

	private GameBoardMapper mapper;

	private static List<GameBoard> games;

	static {
		resetGames();
	}

	private static void resetGames() {
		games = new ArrayList<>();
		games.add(new GameBoard("Chess"));
		games.add(new GameBoard("Monopolly"));
		games.add(new GameBoard("Scrable"));
	}
	
	@Autowired
	public GameBoardDaoImpl(GameBoardMapper mapper) {
		this.mapper = mapper;
	}

	@Override
	public Optional<GameBoardDto> findById(Long id) {
		Optional<GameBoard> first = findGameById(id);
		return mapper.map(first);
	}

	@Override
	public Long save(GameBoardDto gameDto) {
		GameBoard game = mapper.map(gameDto);
		Optional<GameBoard> foundGame = findGameById(game.getId());
		if (foundGame.isPresent()) {
			games.remove(foundGame.get());
			game = foundGame.get();
		}
		games.add(game);
		return game.getId();
	}

	private Optional<GameBoard> findGameById(Long id) {
		return games.stream().filter(o -> o.getId().equals(id)).findFirst();
	}

	@Override
	public List<GameBoard> findAllGames() {
		return games.stream().collect(Collectors.toList());
	}
}
