package com.challenge.dao;

import java.util.List;
import java.util.Optional;

import com.challenge.entity.User;
import com.challenge.mapper.dto.UserDto;

public interface UserDao {
	Optional<UserDto> findById(Long id);

	Long save(UserDto userDto);

	Long updateUserData(UserDto userDto);

	Long addGameToOwnCollection(Long idUser, Long idGameToAdd);

	Optional<UserDto> showProfileInfo(UserDto userDto);

	List<Long> showUserGameCollection(Long userId);

	List<Long> removeGameFromCollection(Long idUser, Long idGameToAdd);

	List<User> findAllUsers();
}
