package com.challenge.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ChallengeMethodsAspect {
	private static final Logger LOGGER = LoggerFactory.getLogger(ChallengeMethodsAspect.class);

	@Around("@annotation(LoggingAnnotation)")
	public Object loggingAnnotation(ProceedingJoinPoint joinPoint) throws Throwable {
		Object proceed = joinPoint.proceed();
		LOGGER.info("Method has been executed: " + joinPoint.toString());
		return proceed;
	}
}
