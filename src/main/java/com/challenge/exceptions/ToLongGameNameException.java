package com.challenge.exceptions;

public class ToLongGameNameException extends Exception {
	public ToLongGameNameException() {
		super("Name game is to loong, please set less than 20 characters!");
	}
}
