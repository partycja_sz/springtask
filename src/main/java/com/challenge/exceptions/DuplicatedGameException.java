package com.challenge.exceptions;

public class DuplicatedGameException extends Exception {

	public DuplicatedGameException() {
		super("Game already exists in Your set!");
	}
}
