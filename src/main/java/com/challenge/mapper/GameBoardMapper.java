package com.challenge.mapper;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.challenge.entity.GameBoard;
import com.challenge.mapper.dto.GameBoardDto;

@Component
public class GameBoardMapper {

	public Optional<GameBoardDto> map(Optional<GameBoard> game) {
		GameBoardDto gameDto = new GameBoardDto();
		if (game.isPresent()) {
			gameDto.setName(game.get().getName());
			gameDto.setId(game.get().getId());
		}
		return Optional.ofNullable(gameDto);
	}

	public GameBoard map(GameBoardDto gameDto) {
		GameBoard game = new GameBoard();
		setGameIdIfPresent(gameDto, game);
		game.setName(gameDto.getName());
		return game;
	}

	private void setGameIdIfPresent(GameBoardDto gameDto, GameBoard game) {
		if (gameDto.getId() != null) {
			game.setId(gameDto.getId());
		}
	}
}
