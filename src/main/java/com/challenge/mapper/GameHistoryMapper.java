package com.challenge.mapper;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.challenge.entity.GameHistory;
import com.challenge.mapper.dto.GameHistoryDto;

@Component
public class GameHistoryMapper {
	public Optional<GameHistoryDto> map(Optional<GameHistory> gameHistory) {
		GameHistoryDto gameHistoryDto = new GameHistoryDto();
		if (gameHistory.isPresent()) {
			gameHistoryDto.setIdFirstPlayer(gameHistory.get().getIdFirstPlayer());
			gameHistoryDto.setIdSecondPlayer(gameHistory.get().getIdSecondPlayer());
			gameHistoryDto.setIdFirstPlayer(gameHistory.get().getIdFirstPlayer());
		}
		return Optional.ofNullable(gameHistoryDto);
	}

	public GameHistory map(GameHistoryDto gameHistoryDto) {
		GameHistory gameHistory = new GameHistory();
		gameHistory.setIdFirstPlayer(gameHistoryDto.getIdFirstPlayer());
		gameHistory.setIdSecondPlayer(gameHistoryDto.getIdSecondPlayer());
		gameHistory.setIdGame(gameHistoryDto.getIdGame());
		return gameHistory;
	}
}
