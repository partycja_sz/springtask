package com.challenge.mapper;

import java.util.Optional;
import org.springframework.stereotype.Component;

import com.challenge.entity.User;
import com.challenge.mapper.dto.UserDto;

@Component
public class UserMapper {

	public Optional<UserDto> map(Optional<User> user) {
		UserDto userDto = new UserDto();
		if (user.isPresent()) {
			userDto.setFirstName(user.get().getFirstName());
			userDto.setLastName(user.get().getLastName());
			userDto.setEmail(user.get().getEmail());
			userDto.setMotto(user.get().getMotto());
			userDto.setPassword(user.get().getPassword());
			userDto.setId(user.get().getId());
			userDto.setGameUserCollection(user.get().getGameUserCollection());
		}
		return Optional.ofNullable(userDto);
	}

	public User map(UserDto userDto) {
		User user = new User();
		setUserIdIfPresent(userDto, user);
		user.setFirstName(userDto.getFirstName());
		user.setLastName(userDto.getLastName());
		user.setEmail(userDto.getEmail());
		user.setMotto(userDto.getMotto());
		user.setPassword(userDto.getPassword());
		user.setGameUserCollection(userDto.getGameUserCollection());
		return user;
	}

	private void setUserIdIfPresent(UserDto userDto, User user) {
		if (userDto.getId() != null) {
			user.setId(userDto.getId());
		}
	}
}
