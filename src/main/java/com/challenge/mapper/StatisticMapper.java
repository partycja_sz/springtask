package com.challenge.mapper;

import java.util.Optional;
import org.springframework.stereotype.Component;

import com.challenge.entity.Statistic;
import com.challenge.mapper.dto.StatisticDto;

@Component
public class StatisticMapper {
	public Optional<StatisticDto> map(Optional<Statistic> statistic) {
		StatisticDto statisticDto = new StatisticDto();
		if (statistic.isPresent()) {
			statisticDto.setLevel(statistic.get().getLevel());
			statisticDto.setRankingPosition(statistic.get().getRankingPosition());
			statisticDto.setWinGamesStatistics(statistic.get().getWinGamesStatistics());
			statisticDto.setUserId(statistic.get().getUserId());
		}
		return Optional.ofNullable(statisticDto);
	}

	public Statistic map(StatisticDto statisticDto) {
		Statistic statistic = new Statistic();
		setStatisticIdIfPresent(statisticDto, statistic);
		statistic.setLevel(statisticDto.getLevel());
		statistic.setRankingPosition(statisticDto.getRankingPosition());
		statistic.setWinGamesStatistics(statisticDto.getWinGamesStatistics());
		return statistic;
	}

	private void setStatisticIdIfPresent(StatisticDto statisticDto, Statistic statistic) {
		if (statisticDto.getUserId() != null) {
			statistic.setUserId(statisticDto.getUserId());
		}
	}
}
