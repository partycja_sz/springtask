package com.challenge.mapper.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GameHistoryDto {
	private Long idFirstPlayer;
	private Long idSecondPlayer;
	private Long idGame;
}
