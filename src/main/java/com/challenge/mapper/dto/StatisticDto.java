package com.challenge.mapper.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatisticDto {
	private Long userId;
	private Long level;
	private Long rankingPosition;
	private Long winGamesStatistics;
}
