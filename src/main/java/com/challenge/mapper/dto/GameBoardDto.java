package com.challenge.mapper.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class GameBoardDto {
	private String name;
	private Long id;
}
