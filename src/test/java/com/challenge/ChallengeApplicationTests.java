package com.challenge;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.challenge.dao.GameBoardDao;
import com.challenge.dao.GameHistoryDao;
import com.challenge.dao.StatisticDao;
import com.challenge.dao.UserDao;
import com.challenge.entity.GameBoard;
import com.challenge.entity.GameHistory;
import com.challenge.entity.User;
import com.challenge.exceptions.DuplicatedGameException;
import com.challenge.exceptions.ToLongGameNameException;
import com.challenge.mapper.GameBoardMapper;
import com.challenge.mapper.GameHistoryMapper;
import com.challenge.mapper.StatisticMapper;
import com.challenge.mapper.UserMapper;
import com.challenge.mapper.dto.GameBoardDto;
import com.challenge.mapper.dto.GameHistoryDto;
import com.challenge.mapper.dto.StatisticDto;
import com.challenge.mapper.dto.UserDto;
import com.challenge.service.GameBoardService;
import com.challenge.service.GameHistoryService;
import com.challenge.service.StatisticService;
import com.challenge.service.UserService;
import com.challenge.service.impl.GameBoardServiceImpl;
import com.challenge.service.impl.GameHistoryServiceImpl;
import com.challenge.service.impl.StatisticServiceImpl;
import com.challenge.service.impl.UserServiceImpl;
import com.challenge.service.validator.GameServiceValidator;
import com.challenge.service.validator.UserServiceValidator;

import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ChallengeApplicationTests {

	@Autowired
	private UserDao userDao;
	@Autowired
	private GameBoardDao gameDao;
	@Autowired
	private StatisticDao statisticDao;
	@Autowired
	private GameHistoryDao gameHistoryDao;
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private GameBoardMapper gameMapper;
	@Autowired
	private StatisticMapper statisticMapper;
	@Autowired
	private GameHistoryMapper gameHistoryMapper;
	@Autowired
	private UserServiceValidator userServiceValidator;
	@Autowired
	private GameServiceValidator gameServiceValidator;

	private UserService userService;
	private GameBoardService gameService;
	private StatisticService statisticService;
	private GameHistoryService gameHistoryService;

	@Before
	public void setUp() {
		userService = new UserServiceImpl(userMapper, userDao, gameDao, userServiceValidator);
		gameService = new GameBoardServiceImpl(gameMapper, gameDao, gameServiceValidator);
		statisticService = new StatisticServiceImpl(statisticMapper, statisticDao);
		gameHistoryService = new GameHistoryServiceImpl(gameHistoryMapper, gameHistoryDao);
	}

	@Test
	public void testAddUserEndsWithSuccess() {
		// given
		UserDto userDto = new UserDto();
		userDto.setFirstName("John");
		userDto.setLastName("Lennon");
		userDto.setEmail("john.lennon@capgemini.stk.com");
		userDto.setMotto("?");

		// when
		Long userId = userService.addUser(userDto);

		// then
		assertThat(userId).isNotNull();
	}

	@Test
	public void testAddGameEndsWithSuccess() throws ToLongGameNameException {
		// given
		GameBoardDto gameDto = new GameBoardDto();
		gameDto.setName("Train");

		// when
		Long gameId = gameService.addGame(gameDto);

		// then
		assertThat(gameId).isNotNull();
	}

	@Test(expected = ToLongGameNameException.class)
	public void testAddGameWithToLongNameExpectedToLongGameNameException() throws ToLongGameNameException {
		// given
		GameBoardDto gameDto = new GameBoardDto();
		gameDto.setName("123456789012345678901");

		// when&then
		gameService.addGame(gameDto);
	}

	@Test
	public void testUserCanSeeOwnProfileInformationEndsWithSuccess() {
		// given
		UserDto userDto2 = new UserDto();
		Long id = (long) 2;
		userDto2 = userDao.findById(id).get();

		// when
		Optional<UserDto> userDto = userService.showProfileInfo(userDto2);

		// then
		assertEquals("Clyde", userDto.get().getFirstName());
		assertEquals("Third", userDto.get().getLastName());
		assertEquals("clyde.third@capgemini.jstk.com", userDto.get().getEmail());
		assertEquals("Always look at the bright side of life", userDto.get().getMotto());
		assertEquals("54321", userDto.get().getPassword());
	}

	@Test
	public void testUserCanSeeOwnGamesCollectionEndsWithSuccess() {
		// given
		Long id = 3L;

		// when
		List<Long> userGames = userService.showUserGameCollection(id);

		// then
		assertTrue(1L == userGames.get(0));
		assertTrue(3L == userGames.get(1));
	}

	@Test
	public void testUserCanRemoveGameFromOwnCollectionEndsWithSuccess() {
		// given
		Long userId = 5L;
		Long idGameToRemove = 1L;

		// when
		List<Long> userGames = userService.removeGameFromCollection(userId, idGameToRemove);

		// then
		assertTrue(2L == userGames.get(0));
		assertTrue(1 == userGames.size());
	}

	@Test
	public void testUserChangeEmailEndsWithSuccess() {
		// given
		UserDto userDto = new UserDto();
		String email = "newEmail@capgemini.jstk.com";
		Long id = (long) 1;
		userDto = userDao.findById(id).get();
		userDto.setEmail(email);

		// when
		userService.updateUserData(userDto);

		// then
		assertEquals(email, userDao.findById(id).get().getEmail());
	}

	@Test
	public void testUserChangeLastNameEndsWithSuccess() {
		// given
		UserDto userDto = new UserDto();
		String lastName = "Szwed";
		Long id = (long) 1;
		userDto = userDao.findById(id).get();
		userDto.setLastName(lastName);

		// when
		userService.updateUserData(userDto);

		// then
		assertEquals(lastName, userDao.findById(id).get().getLastName());
	}

	@Test
	public void testUserChangeFirstNameEndsWithSuccess() {
		// given
		UserDto userDto = new UserDto();
		String firstName = "Patricia";
		Long id = (long) 1;
		userDto = userDao.findById(id).get();
		userDto.setFirstName(firstName);

		// when
		userService.updateUserData(userDto);

		// then
		assertEquals(firstName, userDao.findById(id).get().getFirstName());
	}

	@Test
	public void testUserChangeMottoEndsWithSuccess() {
		// given
		UserDto userDto = new UserDto();
		String motto = "i have to change my motto";
		Long id = (long) 1;
		userDto = userDao.findById(id).get();
		userDto.setMotto(motto);

		// when
		userService.updateUserData(userDto);

		// then
		assertEquals(motto, userDao.findById(id).get().getMotto());
	}
	
	@Test
	public void testUserChangePasswordEndsWithSuccess() {
		// given
		UserDto userDto = new UserDto();
		String password = "P@ssw0rd";
		Long id = (long) 1;
		userDto = userDao.findById(id).get();
		userDto.setPassword(password);

		// when
		userService.updateUserData(userDto);

		// then
		assertEquals(password, userDao.findById(id).get().getPassword());
	}

	@Test
	public void testUserAddGameToOwnCollectionEndsWithSuccess() throws DuplicatedGameException {
		// given
		Long idUser = (long) 1;
		Long idGameToAdd = (long) 2;

		// when
		userService.addGame(idUser, idGameToAdd);

		// then
		assertTrue(1 == userDao.findById(idUser).get().getGameUserCollection().size());
		assertTrue(2L == userDao.findById(idUser).get().getGameUserCollection().get(0));
	}

	@Test(expected = DuplicatedGameException.class)
	public void testUserAddGameExistingAlreadyInOwnCollectionEndsWithDuplicatedGameException()
			throws DuplicatedGameException {
		// given
		Long idUser = (long) 6;
		Long idGameToAdd = (long) 1;

		// when&then
		userService.addGame(idUser, idGameToAdd);
	}

	@Test
	public void testUserCanSeeCurrentStatisticsEndsWithSuccess() {
		// given
		Long idUser = 4L;
		Long level = 3L;
		Long ranking = 1L;
		Long winGames = 10L;

		// when
		Optional<StatisticDto> statisticDto = statisticService.showStatistics(idUser);

		// then
		assertEquals(idUser, statisticDto.get().getUserId());
		assertEquals(level, statisticDto.get().getLevel());
		assertEquals(ranking, statisticDto.get().getRankingPosition());
		assertEquals(winGames, statisticDto.get().getWinGamesStatistics());
	}

	@Test
	public void testUserCanSeOwnHistoryGameEndsWithSuccess() {
		// given
		Long idUser = 1L;

		// when
		List<GameHistory> userGameHistory = gameHistoryService.showGameHistoryForUser(idUser);

		// then
		for (GameHistory gameHistory : userGameHistory) {
			assertTrue(idUser == gameHistory.getIdFirstPlayer() || idUser == gameHistory.getIdSecondPlayer());
		}
	}

	@Test
	public void testAddNewHistoryGameEndsWithSuccess() {
		// given
		Long idFirstPlayer = 2L;
		Long idSecondPlayer = 3L;
		Long idGame = 1L;
		GameHistoryDto gameHistoryDto = new GameHistoryDto();
		gameHistoryDto.setIdFirstPlayer(idFirstPlayer);
		gameHistoryDto.setIdSecondPlayer(idSecondPlayer);
		gameHistoryDto.setIdGame(idGame);

		// when
		gameHistoryService.addGameToHistory(gameHistoryDto);

		// then
		assertFalse(gameHistoryDao.showGameHistoryForUser(idFirstPlayer).isEmpty());
	}

	@Test
	public void testAllGamesAreDisplayedEndsWithSuccess() {
		// given
		List<GameBoard> gameBoardList;

		// when
		gameBoardList = gameService.findAllGames();

		// then
		assertTrue(3 == gameBoardList.size());
	}

	@Test
	public void testAllUsersAreDisplayedEndsWithSuccess() {
		// given
		List<User> userList;

		// when
		userList = userService.findAllUsers();

		// then
		assertTrue(7 == userList.size());
	}
}
